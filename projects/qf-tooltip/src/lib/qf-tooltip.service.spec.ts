import { TestBed } from '@angular/core/testing';

import { QfTooltipService } from './qf-tooltip.service';

describe('QfTooltipService', () => {
  let service: QfTooltipService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(QfTooltipService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
