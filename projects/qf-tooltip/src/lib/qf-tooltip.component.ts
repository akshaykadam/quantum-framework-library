import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  Input,
} from '@angular/core';
import { animate, style, transition, trigger } from '@angular/animations';

@Component({
  selector: 'lib-qf-tooltip',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `<div class="qf-tooltip" @rxTooltip>{{ text }}</div>`,
  styles: [
    `
  :host{display: block}
  .qf-tooltip {
   background-color: #61678f;
   color: white;
   font-size: 12px;
   border-radius: 2px;
   width: fit-content;
   text-align: center;
   word-wrap: break-word;
   padding: 0.5rem 0.3rem;
   }
    `,
  ],
  animations: [
    trigger('rxTooltip', [
      transition(':enter', [
        style({ opacity: 0 }),
        animate(300, style({ opacity: 1 })),
      ]),
      transition(':leave', [animate(300, style({ opacity: 0 }))]),
    ]),
  ],
})
export class QfTooltipComponent implements OnInit {
  @Input() text: string;

  constructor() {}

  ngOnInit(): void {}
}
