import {
  ComponentRef,
  Directive,
  ElementRef,
  HostListener,
  Input,
  OnInit,
  OnDestroy
} from '@angular/core';

import {
  ConnectedPosition,
  Overlay,
  OverlayPositionBuilder,
  OverlayRef,
} from '@angular/cdk/overlay';

import {
  ConnectedPositionBottom,
  ConnectedPositionLeft,
  ConnectedPositionRight,
  ConnectedPositionTop,
} from './qf-tooltip.position.ref';

import {ComponentPortal} from '@angular/cdk/portal';

import { QfTooltipComponent } from './qf-tooltip.component';


@Directive({
  selector: '[qf-tooltip]',
})
export class QfTooltipDirective implements OnInit, OnDestroy {
  @Input('qf-tooltip') text: string;
  @Input() position?: 'top' | 'right' | 'bottom' | 'left' = 'top';
  private overlayRef: OverlayRef;

  constructor(
    private overlay: Overlay,
    private overlayPositionBuilder: OverlayPositionBuilder,
    private elementRef: ElementRef
  ) {}

  ngOnInit(): void {
    const connectedPosition: ConnectedPosition =
      this.position === 'top'
        ? ConnectedPositionTop()
        : this.position === 'right'
        ? ConnectedPositionRight(this.elementRef.nativeElement.offsetWidth)
        : this.position === 'bottom'
        ? ConnectedPositionBottom(this.elementRef.nativeElement.offsetHeight)
        : this.position === 'left'
        ? ConnectedPositionLeft(this.elementRef.nativeElement.offsetWidth)
        : ConnectedPositionTop();

    const positionStrategy = this.overlayPositionBuilder
      .flexibleConnectedTo(this.elementRef)
      .withPositions([connectedPosition]);

    this.overlayRef = this.overlay.create({ positionStrategy });
  }

  @HostListener('mouseenter')
  @HostListener('focus')
  show(): void {
    if (this.overlayRef && !this.overlayRef.hasAttached()) {
      const tooltipRef: ComponentRef<QfTooltipComponent> = this.overlayRef.attach(
        new ComponentPortal(QfTooltipComponent)
      );
      tooltipRef.instance.text = this.text;
    }
  }

  @HostListener('mouseout')
  @HostListener('focusout')
  @HostListener('click')
  hide(): void {
    if (this.overlayRef) {
      this.overlayRef.detach();
    }
  }

  ngOnDestroy(): void {
    this.hide();
  }
}
