import {ConnectedPosition} from '@angular/cdk/overlay';

export function ConnectedPositionTop(offsetY: number | null = 0): ConnectedPosition {
  return {
    originX: 'center',
    originY: 'top',
    overlayX: 'center',
    overlayY: 'bottom',
    offsetY: offsetY - 10
  };
}

export function ConnectedPositionRight(offsetX: number | null = 0): ConnectedPosition {
  return {
    originX: 'start',
    originY: 'center',
    overlayX: 'end',
    overlayY: 'center',
    offsetX: offsetX * 2 + 5
  };
}

export function ConnectedPositionBottom(offsetY: number | null = 0): ConnectedPosition {
  return {
    originX: 'start',
    originY: 'bottom',
    overlayX: 'center',
    overlayY: 'bottom',
    offsetY: offsetY + 5
  };
}

export function ConnectedPositionLeft(offsetX: number | null = 0): ConnectedPosition {
  return {
    originX: 'center',
    originY: 'center',
    overlayX: 'start',
    overlayY: 'center',
    offsetX: -offsetX - 5
  };
}
