import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QfTooltipComponent } from './qf-tooltip.component';

describe('QfTooltipComponent', () => {
  let component: QfTooltipComponent;
  let fixture: ComponentFixture<QfTooltipComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QfTooltipComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QfTooltipComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
