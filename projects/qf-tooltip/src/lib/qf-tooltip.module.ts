import { NgModule } from '@angular/core';
import { QfTooltipComponent } from './qf-tooltip.component';
import { QfTooltipDirective } from './qf-tooltip.directive';
import { OverlayModule } from '@angular/cdk/overlay';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {BrowserModule} from '@angular/platform-browser';

@NgModule({
  declarations: [QfTooltipComponent, QfTooltipDirective],
  imports: [
    OverlayModule,
    BrowserAnimationsModule,
    BrowserModule
  ],
  exports: [
    QfTooltipComponent,
    QfTooltipDirective,
  ]
})
export class QfTooltipModule { }
