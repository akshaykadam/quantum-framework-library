import { TestBed } from '@angular/core/testing';

import { QfToggleService } from './qf-toggle.service';

describe('QfToggleService', () => {
  let service: QfToggleService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(QfToggleService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
