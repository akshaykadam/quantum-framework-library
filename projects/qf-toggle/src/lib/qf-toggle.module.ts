import { NgModule } from '@angular/core';
import { QfToggleComponent } from './qf-toggle.component';
import { UiSwitchModule } from 'ngx-ui-switch';

@NgModule({
  declarations: [QfToggleComponent],
  imports: [
    UiSwitchModule
  ],
  exports: [QfToggleComponent, UiSwitchModule]
})
export class QfToggleModule { }
