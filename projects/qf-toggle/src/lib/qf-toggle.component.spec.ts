import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QfToggleComponent } from './qf-toggle.component';

describe('QfToggleComponent', () => {
  let component: QfToggleComponent;
  let fixture: ComponentFixture<QfToggleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QfToggleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QfToggleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
