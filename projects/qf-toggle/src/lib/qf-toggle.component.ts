import {
  Component,
  OnInit,
  OnChanges,
  SimpleChanges,
  EventEmitter,
  Input,
  Output,
} from '@angular/core';

@Component({
  selector: 'qf-toggle',
  template: `
    <div class="qf-switch">
      <span class="__status">{{
        checked ? checkedLabel : unCheckedLabel
      }}</span>
      <ui-switch
        [checked]="checked"
        [size]="size"
        checkedTextColor="#ffffff"
        [color]="color"
        (change)="onChange($event)"
      ></ui-switch>
    </div>
  `,
  styles: [],
})
export class QfToggleComponent implements OnInit, OnChanges {
  @Input() checked = false;
  @Input() size?: 'small' | 'medium' | 'large' = 'small';
  @Input() checkedLabel?: string | null = null;
  @Input() unCheckedLabel?: string | null = null;
  @Input() color = 'rgb(75,202,129)';
  @Input() data: any;
  @Output() changeEvent: EventEmitter<any> = new EventEmitter<any>();

  constructor() {}

  ngOnInit(): void {}

  ngOnChanges(changes: SimpleChanges): void {}

  public onChange = (event: any) => {
    this.checked = event;
    this.changeEvent.emit({ event, data: this.data });
  };
}
