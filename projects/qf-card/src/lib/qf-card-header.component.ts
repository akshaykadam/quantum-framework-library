import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'qf-card-header',
  template: `
      <ng-content></ng-content>
  `,
  styles: [],
})
export class QfCardHeaderComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}
}
