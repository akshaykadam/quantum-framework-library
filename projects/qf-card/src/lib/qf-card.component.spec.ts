import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QfCardComponent } from './qf-card.component';

describe('QfCardComponent', () => {
  let component: QfCardComponent;
  let fixture: ComponentFixture<QfCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QfCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QfCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
