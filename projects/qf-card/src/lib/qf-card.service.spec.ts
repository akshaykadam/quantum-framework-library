import { TestBed } from '@angular/core/testing';

import { QfCardService } from './qf-card.service';

describe('QfCardService', () => {
  let service: QfCardService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(QfCardService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
