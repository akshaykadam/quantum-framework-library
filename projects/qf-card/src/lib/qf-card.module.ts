import { NgModule } from '@angular/core';
import { QfCardComponent } from './qf-card.component';
import { QfCardHeaderComponent } from './qf-card-header.component';
import { QfCardFooterComponent } from './qf-card-footer.component';


@NgModule({
  declarations: [QfCardComponent, QfCardHeaderComponent, QfCardFooterComponent],
  imports: [
  ],
  exports: [QfCardComponent, QfCardHeaderComponent, QfCardFooterComponent]
})
export class QfCardModule { }
