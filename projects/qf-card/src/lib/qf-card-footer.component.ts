import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'qf-card-footer',
  template: `
      <ng-content></ng-content>
  `,
  styles: [],
})
export class QfCardFooterComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}
}
