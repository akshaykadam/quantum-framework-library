import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'qf-card',
  template: `
    <div class="card qf-card">
      <div class="card-body">
        <ng-content></ng-content>
      </div>
    </div>
  `,
  styles: [],
})
export class QfCardComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}
}
