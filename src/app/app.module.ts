import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { QfTooltipModule } from 'qf-tooltip';
import { QfCardModule } from 'qf-card';
import { QfToggleModule } from 'qf-toggle';
import { from } from 'rxjs';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    QfTooltipModule,
    QfCardModule,
    QfToggleModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
